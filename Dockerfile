### docker build . -t jontow:erlang22
### docker run -v ~/GIT-je/scripts/code_examples/erlang/erlmon:/web -it jontow:erlang22

FROM erlang:22-alpine
USER root

RUN apk add alpine-sdk && \
    mkdir /.cache && \
    chgrp 0 /.cache && \
    chmod g+w /.cache && \
    mkdir /.ssh && \
    chown 1000:0 /.ssh && \
    chmod 700 /.ssh

EXPOSE 8080
USER 1000:0
CMD ["/bin/sh"]
