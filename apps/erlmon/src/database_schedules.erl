%%%-------------------------------------------------------------------
%% @doc erlmon database_schedules
%% @end
%%%-------------------------------------------------------------------

-module(database_schedules).
-export([get_actionable_schedules/0,
         get_all/0,
         get_ready_once/0,
         get_ready_recur/0,
         get_schedule/1]).

-include_lib("stdlib/include/qlc.hrl").
-include("config.hrl").
-include("data.hrl").

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% get_actionable_schedules() -> {ok, [#erlmon_schedule, #erlmon_schedule, ...]}
%%
%% Retrieve list of schedules that are ready to be processsed: this means that
%% their specified timeframe is currently happening or has very recently passed
%% without being acted upon
%%
get_actionable_schedules() ->
    {ok, Once} = get_ready_once(),
    {ok, Recur} = get_ready_recur(),
    {ok, Once ++ Recur}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% get_ready_once() -> {ok, [#erlmon_schedule, ...]}
%%
get_ready_once() ->
    F = fun() ->
                qlc:e(qlc:q([Result || Result <- mnesia:table(erlmon_schedule),
                             (Result#erlmon_schedule.active == true) and
                             (Result#erlmon_schedule.sched_type == once)]))
        end,

    {atomic, ReadyList} = mnesia:transaction(F),
    filter_sched_once_ready(ReadyList).
    %% TODO: sort the return list by time

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% filter_sched_once_ready(List) -> {ok, List}
%%
filter_sched_once_ready(ReadyList) -> filter_sched_once_ready(ReadyList, []).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% filter_sched_once_ready(List, List) -> {ok, List}
%%
filter_sched_once_ready([], ReadyList) -> {ok, ReadyList};
filter_sched_once_ready([H|T], ReadyList) ->
    SchedName = H#erlmon_schedule.sched_name,
    SchedPid = H#erlmon_schedule.pid,
    NowSecs = erlang:system_time(second),
    GregorianEpoch = calendar:datetime_to_gregorian_seconds({{1970,1,1}, {0,0,0}}),
    ParsedGregSecs = calendar:datetime_to_gregorian_seconds(iso8601:parse(H#erlmon_schedule.sched_time)),
    ParsedSecs = ParsedGregSecs - GregorianEpoch,
    Diff = ParsedSecs - NowSecs,
    case SchedPid of
        %% SchedPid is unset, so event is not yet handled
        null ->
            case Diff of
                _ when Diff < ?ONCE_LOWER_TIME_DIFF ->
                    io:format("DEBUG: --> '~p' Time Diff: ~p (IN THE PAST, BUT UNHANDLED)~n", [SchedName, Diff]),
                    filter_sched_once_ready(T, [H|ReadyList]);
                _ when Diff >= ?ONCE_LOWER_TIME_DIFF, Diff =< ?ONCE_UPPER_TIME_DIFF ->
                    io:format("DEBUG: '~p' Time Diff: ~p (RIGHT NOW)~n", [SchedName, Diff]),
                    filter_sched_once_ready(T, [H|ReadyList]);
                _ when Diff > ?ONCE_UPPER_TIME_DIFF ->
                    io:format("DEBUG: '~p' Time Diff: ~p (IN THE FUTURE)~n", [SchedName, Diff]),
                    filter_sched_once_ready(T, ReadyList)
            end;
        _ ->
            %io:format("DEBUG: --> '~p' Time Diff: ~p (ALREADY HANDLED)~n", [SchedName, Diff]),
            filter_sched_once_ready(T, ReadyList)
    end.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% get_ready_recur() -> {atomic, [#erlmon_schedule, ...]}
%%
get_ready_recur() ->
    F = fun() ->
                qlc:e(qlc:q([Result || Result <- mnesia:table(erlmon_schedule),
                             (Result#erlmon_schedule.active == true) and
                             (Result#erlmon_schedule.sched_type == recur)]))
        end,
    {atomic, ReadyList} = mnesia:transaction(F),
    %{ok, ReadyList}.
    filter_sched_recur_ready(ReadyList).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% filter_sched_recur_ready(List) -> {ok, List}
%%
filter_sched_recur_ready(ReadyList) -> filter_sched_recur_ready(ReadyList, []).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% filter_sched_recur_ready(List, List) -> {ok, List}
%%
filter_sched_recur_ready([], ReadyList) -> {ok, ReadyList};
filter_sched_recur_ready([H|T], ReadyList) ->
    SchedName = H#erlmon_schedule.sched_name,
    SchedPid = H#erlmon_schedule.pid,
    if
        %% responsible pid not set
        SchedPid == null ->
            filter_sched_recur_ready(T, [H|ReadyList]);

        %% responsible pid is set!
        %% skip this sched entry, it has already been handled.
        true ->
            %% check responsible process aliveness!
            case is_process_alive(SchedPid) of
                true ->
                    %io:format("DEBUG: --> '~p' already being handled by ~p)~n", [SchedName, SchedPid]),
                    filter_sched_recur_ready(T, ReadyList);
                false ->
                    io:format("INFO: Recurring schedule '~p' handled by dead process (~p), marking ready for respawn..~n", [SchedName, SchedPid]),
                    %% clear pid and add to ReadyList
                    NewSchedRec = #erlmon_schedule{
                                     sched_name = H#erlmon_schedule.sched_name,
                                     sched_type = H#erlmon_schedule.sched_type,
                                     sched_time = H#erlmon_schedule.sched_time,
                                     message = H#erlmon_schedule.message,
                                     pid = null,
                                     active = H#erlmon_schedule.active},
                    filter_sched_recur_ready(T, [NewSchedRec|ReadyList])
            end
    end.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% get_all() -> {ok, [#erlmon_schedule, ...]}
%%
get_all() ->
    database:get_all(erlmon_schedule).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% get_schedule(String) -> {atomic, [#erlmon_schedule]}
%%
get_schedule(SName) ->
    %% Fetch complete schedule record
    FunR = fun() ->
                qlc:e(qlc:q([Result || Result <- mnesia:table(erlmon_schedule),
                                       Result#erlmon_schedule.sched_name == SName]))
        end,
    mnesia:transaction(FunR).
