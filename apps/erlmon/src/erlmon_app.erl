%%%-------------------------------------------------------------------
%% @doc erlmon public API
%% @end
%%%-------------------------------------------------------------------

-module(erlmon_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
    Dispatch = cowboy_router:compile([
        {'_', [
            {"/", main_handler, []},
            {"/index.html", main_handler, []},
            {'_', notfound_handler, []}
        ]}
    ]),
    {ok, _} = cowboy:start_clear(erlmon_http_listener,
        [{port, 8080}],
        #{env => #{dispatch => Dispatch}}
    ),

    database:init(),

    erlmon_sup:start_link().

stop(_State) ->
    ok = cowboy:stop_listener(http).

%% internal functions
