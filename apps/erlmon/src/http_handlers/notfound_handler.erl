-module(notfound_handler).
-export([init/2]).

init(Req0, State) ->
    io:format("LOG: got 'notfound' request ~p, ~p~n", [Req0, State]),
    Req = cowboy_req:reply(404,
        #{<<"content-type">> => <<"text/plain">>},
        <<"Confusion reigns supreme.\n">>,
        Req0),
    {ok, Req, State}.
