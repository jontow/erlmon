-module(main_handler).
-export([init/2]).

init(Req0, State) ->
    io:format("LOG: got 'main' request ~p, ~p~n", [Req0, State]),
    Req = cowboy_req:reply(200,
        #{<<"content-type">> => <<"text/plain">>},
        <<"Welcome home.\n">>,
        Req0),
    {ok, Req, State}.
