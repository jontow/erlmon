-module(sched_recur).
-export([handle/1]).

-include_lib("stdlib/include/qlc.hrl").
-include("data.hrl").

%% Called without responsible Pid, set ourself and continue
handle({erlmon_schedule,SName,recur,Timestamp,Message,null,Active}) ->
    %% Now build a new schedule record with updated 'pid' field to denote that it was handled.
    NewSchedRec = #erlmon_schedule{
                     sched_name = SName,
                     sched_type = recur,
                     sched_time = Timestamp,
                     message = Message,
                     pid = self(),
                     active = Active},
    FunW = fun() ->
                mnesia:write(NewSchedRec)
           end,
    mnesia:transaction(FunW),
    handle(NewSchedRec);

handle({erlmon_schedule,SName,recur,Timestamp,Message,Pid,Active}) ->
    SchedRec = #erlmon_schedule{
        sched_name = SName,
        sched_type = recur,
        sched_time = Timestamp,
        message = Message,
        pid = Pid,
        active = Active},

    case re:run(Timestamp,
                "^every ([0-9]+) (seconds|minutes|hours|days)$", [global, {capture, all, list}]) of
        {match, [[_All, Interval, Unit]]} ->
            %io:format("DEBUG: Gonna do something every (~p) (~p)~n", [Interval, Unit]),
            EpochNow = lib:epoch_now(),
            Mx = case Unit of
                "seconds" ->
                    list_to_integer(Interval) * 1;
                "minutes" ->
                    list_to_integer(Interval) * 60;
                "hours" ->
                    list_to_integer(Interval) * 3600;
                "days" ->
                    list_to_integer(Interval) * 86400;
                _ ->
                    io:format("ERROR: sched_recur:handle(~p): unknown unit: ~p~n", [SName, Unit])
            end,
            Calc = EpochNow rem Mx,

            if Calc == 0 -> action(SchedRec);
               Calc /= 0 -> noop
            end;
        nomatch ->
            io:format("DEBUG: Didn't get a match from: (~p)~n", [Timestamp])
    end,

    %% Sleep for a second to avoid endless spinning
    timer:sleep(1000),
    handle(SchedRec).

action(SchedRec) ->
    ISONow = lib:iso8601_now(),
    SName = SchedRec#erlmon_schedule.sched_name,
    Message = SchedRec#erlmon_schedule.message,

    {atomic, _EventID} = event:log_event(SName, ISONow, scheduled, info, Message),

    {atomic, AlertCfg} = alerter:get_alert_cfg(SchedRec#erlmon_schedule.sched_name),
    case AlertCfg of
        [] -> noop;
        _ ->
            io:format("DEBUG: AlertCfg = ~p~n", [AlertCfg]),
            alerter:send_alert(Message, AlertCfg)
    end.
