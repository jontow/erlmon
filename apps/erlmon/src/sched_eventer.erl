%%%-------------------------------------------------------------------
%% @doc erlmon schedule eventer
%% @end
%%%-------------------------------------------------------------------

-module(sched_eventer).
-behavior(gen_server).

-export([code_change/3,
         init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         restart/0,
         start/0,
         start_link/0,
         stop/0,
         terminate/2]).

-define(TIMER_INTERVAL, 500).

start_link() ->
    start().

start() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

restart() ->
    stop(),
    start(),
    {ok, restart}.

stop() ->
    gen_server:call(?MODULE, stop).

%%       -> {ok, State}.
init([]) ->
    io:format("DEBUG: sched_eventer:init()~n"),
    spawn_link(fun() -> cron_start() end),
    {ok, self()}.

handle_call(Request, From, State) -> {reply, {Request, From, something}, State}.

handle_cast(_Msg, State) -> {noreply, State}.
handle_info(_Info, State) -> {noreply, State}.
terminate(_Reason, _State) -> ok.
code_change(_OldVsn, State, _Extra) -> {ok, State}.

%% Entry point: called to avoid waiting for full timeout period
%% before getting first cron_tick()
cron_start() ->
    %% Avoid coming online before database does by sleeping
    timer:sleep(1000),
    cron_tick(),
    cron_loop().

%% Loop with a preset timer.
cron_loop() ->
    receive
        {From, Msg} ->
            io:format("DEBUG: sched_eventer:cron_loop() got message (~p) from (~p)~n", [Msg, From]),
            cron_loop()
    after ?TIMER_INTERVAL ->
        cron_tick(),
        cron_loop()
    end.

%% Get schedules, spawn handlers
cron_tick() ->
    case database_schedules:get_actionable_schedules() of
        %{ok, []} -> io:format("DEBUG: sched_eventer:cron_loop() TICK!~n");
        {ok, []} -> noop;
        {ok, Schedules} -> handle_schedules(Schedules)
    end.

handle_schedules([]) ->
    %io:format("DEBUG: sched_eventer:handle_schedules() Finished with schedules.~n"),
    ok;
handle_schedules([H|T]) ->
    case H of
        {erlmon_schedule, _SName, once, _, _, _, _} ->
            %io:format("DEBUG: sched_eventer:handle_schedules() got single-use sched ~p~n", [SName]),
            spawn(fun() -> sched_once:handle(H) end);
        {erlmon_schedule, _SName, recur, _, _, _, _} ->
            %io:format("DEBUG: sched_eventer:handle_schedules() got recurring sched ~p~n", [SName]),
            %% Should we start 'recur' schedules under their own supervisor to allow for restarts?
            spawn(fun() -> sched_recur:handle(H) end)
    end,
    handle_schedules(T).
