%% Valid system users/alert recipients
-record(erlmon_user, {username,                % string, regex: /^[A-Za-z0-9-_.]$/
                      password,                % string, md5(?) hashed password.
                      email,                   % string, format varies
                      pager}).                 % string, format varies (email format)

%% Maps which alerts (raised events) notify which users
-record(erlmon_alert_config, {alert_id,        % string, uuid
                              event_name,      % string, regex: /^[A-Za-z0-9-_.]$/
                              recipients}).    % list of {'username',method} tuples

%% Raw events get logged here
-record(erlmon_event, {event_id,               % string, uuid
                       event_name,             % string, regex: /^[A-Za-z0-9-_.]$/
                       timestamp,              % string, iso8601 format
                       type,                   % atom, (triggered|scheduled)
                       severity,               % atom, (info|warn|crit|unknown)
                       alerted,                % atom, boolean (true|false)
                       acked,                  % atom, boolean (true|false)
                       message}).              % string, free form

%% Schedules defined here
-record(erlmon_schedule, {sched_name,         % string, regex: /^sched_[A-Za-z0-9-_.]$/
                          sched_type,         % atom, (once|recur)
                          sched_time,         % string, format varies by sched_type:
                                              %   'once' (fixed) time: iso8601
                                              %   'recur'ring time: /^every [0-9]+ (seconds|minutes|hours|days)$/
                          message,            % string, free form
                          pid,                % Pid of responsible process
                          active}).           % atom, boolean (true|false)
