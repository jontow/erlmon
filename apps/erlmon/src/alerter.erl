-module(alerter).
-export([get_alert_cfg/1,
         send_alert/2,
         send_email_alert/3,
         send_pager_alert/3]).

-include_lib("stdlib/include/qlc.hrl").
-include("config.hrl").
-include("data.hrl").

%% get_alert_cfg(String) -> {atomic, [{#erlmon_alert_config,...}]}
get_alert_cfg(EvName) ->
    %% Fetch alert config for this event, can return multiple rows
    FunA = fun() ->
                qlc:e(qlc:q([Result || Result <- mnesia:table(erlmon_alert_config),
                                       Result#erlmon_alert_config.event_name == EvName]))
           end,
    mnesia:transaction(FunA).

send_alert(_Msg, []) ->
    {ok, alerted};
send_alert(Msg, [H|T]) ->
    io:format("DEBUG: alerter:send_alert(~p, (~p)~n", [Msg, H]),
    EvName = H#erlmon_alert_config.event_name,
    RecipList = H#erlmon_alert_config.recipients,

    %% Send individual alert to list of recipients
    ok = send_alert(EvName, Msg, RecipList),

    %% Continue processing alert configs
    send_alert(Msg, T).

send_alert(_EvName, _Msg, []) ->
    ok;
send_alert(EvName, Msg, [H|T]) ->
    case H of
        {To, email} ->
            io:format("DEBUG: send_alert(email, ~p, ~p, ~p)~n", [To, EvName, Msg]),
            send_email_alert([To], EvName, Msg);
        {To, pager} ->
            io:format("DEBUG: send_alert(pager, ~p, ~p, ~p)~n", [To, EvName, Msg]),
            send_pager_alert([To], EvName, Msg)
    end,
    send_alert(EvName, Msg, T).

send_email_alert(Recipients, EvName, Message) ->
    gen_smtp_client:send({?SMTP_FROM, Recipients, "From: " ++ ?SMTP_FROM ++ "\r\nSubject: erlmon alert for " ++ EvName ++ "\r\n\r\n" ++ Message}, ?SMTP_OPTIONS).

send_pager_alert(Recipients, EvName, Message) ->
    gen_smtp_client:send({?SMTP_FROM, Recipients, "From: " ++ ?SMTP_FROM ++ "\r\n\r\n" ++ EvName ++ ": " ++ Message}, ?SMTP_OPTIONS).
