-module(sched_once).
-export([handle/1]).

-include_lib("stdlib/include/qlc.hrl").
-include("data.hrl").

handle({erlmon_schedule,SName,once,_Time,Message,_Pid,_Active}) ->
    Now = lib:iso8601_now(),
    {atomic, EventID} = event:log_event(SName, Now, scheduled, info, Message),
    io:format("DEBUG: sched_once:handle(~p/~p)~n", [SName, EventID]),

    %% Fetch complete schedule record
    {atomic, [SchedRec]} = database_schedule:get_schedule(SName),
    {atomic, AlertCfg} = alerter:get_alert_cfg(SName),
    case AlertCfg of
        [] -> noop;
        _ ->
            io:format("DEBUG: AlertCfg = ~p~n", [AlertCfg]),
            alerter:send_alert(Message, AlertCfg)
    end,

    %% Now build a new schedule record with updated 'pid' field to denote that it was handled.
    NewSchedRec = #erlmon_schedule{
                     sched_name = SchedRec#erlmon_schedule.sched_name,
                     sched_type = SchedRec#erlmon_schedule.sched_type,
                     sched_time = SchedRec#erlmon_schedule.sched_time,
                     message = SchedRec#erlmon_schedule.message,
                     pid = self(),
                     active = SchedRec#erlmon_schedule.active},

    io:format("DEBUG: NewSchedRec: ~p~n", [NewSchedRec]),

    FunW = fun() ->
                mnesia:write(NewSchedRec)
           end,
    mnesia:transaction(FunW).
