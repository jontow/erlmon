-module(lib).
-export([epoch_now/0,
         iso8601_now/0]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% Time-related helper functions
%%
epoch_now() ->
    erlang:system_time(second).

iso8601_now() ->
    iso8601:format(calendar:universal_time()).
