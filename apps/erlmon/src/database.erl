%%%-------------------------------------------------------------------
%% @doc erlmon database
%% @end
%%%-------------------------------------------------------------------

-module(database).
-export([init/0,
         get_all/1,
         get_all_keys/1,
         get_row/2,
         get_user/1,
         upsert_row/1]).

-include_lib("stdlib/include/qlc.hrl").
-include("data.hrl").

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% init() -> {ok, db_initialized}
%%
init() ->
    io:format("DEBUG: database:init()~n"),
    mnesia:create_schema([node()]),
    mnesia:change_table_copy_type(schema, node(), disc_copies),
    mnesia:start(),
    create_tables(),
    {ok, db_initialized}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% create_tables() -> {ok, created}
%%
create_tables() ->
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%
    %% `erlmon_user` table
    %%
    try
        mnesia:table_info(type, erlmon_user),
        io:format("DEBUG: create_table(erlmon_user): table already exists.~n")
    catch
        exit: _ ->
            mnesia:create_table(erlmon_user, [{attributes, record_info(fields, erlmon_user)},
                                       {type, set}, {disc_copies, [node()]}])
    end,

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%
    %% `erlmon_alert_config` table
    %%
    try
        mnesia:table_info(type, erlmon_alert_config),
        io:format("DEBUG: create_table(erlmon_alert_config): table already exists.~n")
    catch
        exit: _ ->
            mnesia:create_table(erlmon_alert_config, [{attributes, record_info(fields, erlmon_alert_config)},
                                               {type, set}, {disc_copies, [node()]}])
    end,

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%
    %% `erlmon_event` table
    %%
    try
        mnesia:table_info(type, erlmon_event),
        io:format("DEBUG: create_table(erlmon_event): table already exists.~n")
    catch
        exit: _ ->
            mnesia:create_table(erlmon_event, [{attributes, record_info(fields, erlmon_event)},
                                               {type, set}, {disc_copies, [node()]}])
    end,

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%
    %% `erlmon_schedule` table
    %%
    try
        mnesia:table_info(type, erlmon_schedule),
        io:format("DEBUG: create_table(erlmon_schedule): table already exists.~n")
    catch
        exit: _ ->
            mnesia:create_table(erlmon_schedule, [{attributes, record_info(fields, erlmon_schedule)},
                                           {type, set}, {disc_copies, [node()]}])
    end,

    {ok, created}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% get_all(Table) -> {atomic, [Key, ...]}
%%
get_all(Table) ->
    {atomic, KeyList} = get_all_keys(Table),
    get_all(Table, KeyList, []).

get_all(_, [], Rows) ->
    {ok, Rows};
get_all(Table, [Key|KeyTail], Rows) ->
    {atomic, Row} = get_row(Table, Key),
    get_all(Table, KeyTail, [Row|Rows]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% get_all_keys(Table) -> {atomic, [Key, ...]}
%%
get_all_keys(Table) ->
    F = fun() ->
                mnesia:all_keys(Table)
        end,
    mnesia:transaction(F).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% get_row(Table, Key) -> {atomic, Record}
%%
get_row(Table, Key) ->
    F = fun() ->
                mnesia:read(Table, Key)
        end,
    % Carefully strip the outer list [] off returned Row:
    {atomic, [Row]} = mnesia:transaction(F),
    {atomic, Row}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 
%% get_user(String) -> {ok, #erlmon_user)}
%%                  -> {error, nosuchuser}
%%
get_user(Username) ->
    F = fun() ->
                qlc:e(qlc:q([Result || Result <- mnesia:table(erlmon_user),
                             Result#erlmon_user.username == Username]))
        end,
    case mnesia:transaction(F) of
        {atomic, []} ->
            {error, nosuchuser};
        {atomic, [RUser]} ->
            {ok, RUser}
    end.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% upsert_row(Record) -> {atomic, [Row]}
%%
%% Updates a table row with passed in record "Row": assumes table by record
%% type.  If key does not exist, inserts a new row with supplied data.
%%
upsert_row(Row) ->
    F = fun() ->
                mnesia:write(Row)
        end,
    mnesia:transaction(F).
