-module(event).
-export([log_event/1,
         log_event/5,
         log_event/7]).

-include_lib("stdlib/include/qlc.hrl").
-include("data.hrl").

%%
%% Entire event record passed, 'upsert' it.
%%
%% log_event(Record#erlmon_event) -> {atomic, Integer}
log_event(Event) ->
    io:format("DEBUG: log_event(~p)~n", [Event]),
    F = fun() ->
                mnesia:write(Event)
        end,
    mnesia:transaction(F),
    {atomic, Event#erlmon_event.event_id}.

%%
%% Bare form: no ID, Alerted, or Acked fields supplied
%%
log_event(EvName, EvTimestamp, EvType, EvSeverity, EvMessage) ->
    log_event(EvName,EvTimestamp,EvType,EvSeverity,EvMessage,false,false).

%% Full form: initial insert (no ID supplied)
log_event(EvName, EvTimestamp, EvType, EvSeverity, EvMessage, EvAlerted, EvAcked) ->
    EvID = get_next_event_id(),
    %io:format("DEBUG: log_event(~p, ~p, ~p, ~p, ~p, ~p, ~p, ~p)~n", [EvID, EvName, EvTimestamp, EvType, EvSeverity, EvMessage, EvAlerted, EvAcked]),

    Event = #erlmon_event{
               event_id = EvID,
               event_name = EvName,
               timestamp = EvTimestamp,
               type = EvType,
               severity = EvSeverity,
               alerted = EvAlerted,
               acked = EvAcked,
               message = EvMessage},

    log_event(Event).

get_next_event_id() ->
    uuid:to_string(uuid:uuid4()).

%% Bad news: this is prone to race conditions, should be avoided
%get_next_event_id() ->
%    F = fun() ->
%                qlc:e(qlc:q([Result#erlmon_event.event_id || Result <- mnesia:table(erlmon_event)]))
%        end,
%    {atomic, EvID} = mnesia:transaction(F),
%
%    case EvID of
%        [] -> 0;
%        Other ->
%            lists:nth(1, lists:reverse(lists:sort(Other))) + 1
%    end.
